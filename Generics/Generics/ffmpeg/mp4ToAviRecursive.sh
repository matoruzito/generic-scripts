for i in *.mp4;
  do name=`echo "$i" | cut -d'.' -f1`
  echo "$name"
   ffmpeg -i "$i" -vcodec copy -acodec copy "${name}.avi"
done
