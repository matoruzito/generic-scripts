#remove old docker versions
sudo apt-get remove docker docker-engine docker.io containerd runc
#update
sudo apt-get update -y
#download tools for https sources
sudo apt install apt-transport-https ca-certificates curl software-properties-common
#download the gpg key of docker
curl -fsSl https://download.docker.com/linux/debian/gpg | sudo apt-key add - 
#add docker repo to sources.list
sudo add-apt-repository "deb [Arch=AMD64] https://download.docker.com/linux/debian bionic stable"
#update again
sudo apt update -y 
#install docker engine
sudo apt-get -y install docker-ce docker-ce-cli containerd.io
#test docker with an example
sudo docker run hello-world
