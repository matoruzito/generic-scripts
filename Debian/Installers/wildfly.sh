HOMEDIR='/home/'$USER'/Downloads/'
FILENAME='wildfly-23.0.0.Final.tar.gz'
FILENAMEGUNZIPEADO='wildfly-23.0.0.Final.tar'
FILENAMEDESTAREADO='wildfly-23.0.0.Final'


gunzip $HOMEDIR$FILENAME
tar xf $HOMEDIR$FILENAMEGUNZIPEADO
sudo groupadd -r wildfly
sudo useradd -r -g wildfly -d /opt/wildfly -s /sbin/nologin wildfly
sudo mv $HOMEDIR$FILENAMEDESTAREADO /opt
sudo ln -s /opt/$FILENAMEDESTAREADO/ /opt/wildfly
sudo chown -RH wildfly: /opt/wildfly
sudo mkdir -p /etc/wildfly
sudo cp /opt/wildfly/docs/contrib/scripts/init.d/wildfly.conf /etc/wildfly/
sudo cp /opt/wildfly/docs/contrib/scripts/init.d/wildfly-init-debian.sh /etc/init.d/wildfly
sudo sed -i -e 's,NAME=wildfly,NAME='wildfly',g' /etc/init.d/wildfly
sudo service wildfly start
