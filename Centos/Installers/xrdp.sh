sudo yum update -y
sudo yum upgrade -y
sudo yum install -y tigervnc-server xrdp
sudo systemctl start xrdp
sudo netstat -antup | grep xrdp
sudo systemctl enable xrdp
sudo firewall-cmd --permanent --add-port=3389/tcp
sudo firewall-cmd --reload
