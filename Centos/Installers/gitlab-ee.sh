sudo yum update -y
sudo dnf install -y curl policycoreutils openssh-server
sudo systemctl enable sshd
sudo systemctl start sshd
sudo firewall-cmd --permanent --add-service=http
sudo firewall-cmd --permanent --add-service=https
sudo systemctl reload firewalld
sudo dnf install postfix
sudo systemctl enable postfix
sudo systemctl start postfix
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash
#Change gitlab.example.com by your dns.
sudo EXTERNAL_URL="http://gitlab.example.com" dnf install -y gitlab-ee